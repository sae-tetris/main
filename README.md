<img src="src/main/resources/factoryfall/images/titre.png" alt="Logo Factory Fall">

# Factory Fall
## Description
La SAÉ du troisième semestre du BUT Informatique nous invite à programmer un jeu d'arcade de notre choix, en suivant les méthodes de travail en équipe AGILE.

## Choix du jeu
*Les premières idées :*

Nous cherchions donc un jeu remplissant plusieurs critères, établis grâce au projet :
facile à comprendre et à jouer (avec peu d’entrées utilisateurs)
interface graphique pauvre, ce n’est pas là-dessus que l’on souhaite s’attarder
grand public et connu, pour ne pas avoir une trop grosse étape de clarification
agréable à jouer, nous avons l’esprit de compétition, on veut pouvoir défier !

Plusieurs jeux se présentent alors. Des piliers du jeu vidéo comme Pac-Man et Tetris remplissent tous les critères. En plus des critères cités précédemment, ces deux jeux sont très colorés et plutôt cubiques, parfaits pour imiter le style des jeux d’arcades.
Nous avons finalement choisi Tetris, pour plusieurs raisons. Ne pas avoir à coder d’intelligence artificielle est un point très important de notre choix. De plus, les membres du groupe ont préféré __Tetris__ par affinité.

## L'équipe
Nous sommes une équipe constituée de 6 personnes :
Alexis Aubert, Matis Byar, Louis Cornet, Valentin Germain, Alban Pierrevelcin, Erwan Vivien.
Alexis occupe le rôle de **Product Owner**. C'est lui qui fait l'interface entre les clients et l'équipe.
Matis occupe le rôle de **Scrum Master**. Il est le "gardien" de la méthode agile et veille à ce que l'équipe la suit. 

## Documentation
L'ensemble du code est documenté. Vous pourrez également retrouver des informations complémentaires sur la rubrique <a href="https://gitlabinfo.iutmontp.univ-montp2.fr/sae-tetris/main/-/wikis/home">Wiki</a>.
